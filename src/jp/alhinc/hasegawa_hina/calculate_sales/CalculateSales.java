package jp.alhinc.hasegawa_hina.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class CalculateSales {
	public static void main(String[] args) {
		Map<String,String> nameMap = new HashMap<>();
		Map<String,Long> saleMap = new HashMap<>();
			//売上集計課題のファイルを開く
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
			//１行ずつ読むreadlineメソッドが用意されている
			//どこでも使えるように初期化してある
		BufferedReader br =null;

		try {	//売上ファイルの中のbranch.lstの指定をする
			File file =new File(args[0], "branch.lst");
			if( !file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
				//１文字ずつ読み込む
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null){
				String[] branches = line.split(",");
				if(branches.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				if((!branches[0].matches("^[0-9]{3}$"))){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				nameMap.put(branches[0],branches[1]);
				saleMap.put(branches[0],0L);
			}
			//エラーが出た場合
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		} finally {
			if(br != null) {
			try {
				br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
		File directory =new File(args[0]);
			//売上ファイルを配列にする
		File[] files = directory.listFiles();
			//売上ファイル用のリストを作る
		List<String> list = new ArrayList<String>();
			//なくなるまで繰り返す
		for(int i = 0; i < files.length; i++) {
				String filename = files[i].getName();
					//８桁の数字且つrcd拡張子
				if(filename.matches("[0-9]{8}.*rcd") && files[i].isFile()){
						//作ったリストに集めたやつを入れる
					list.add(files[i].getName());
				}
		}
		for(int i = 0; i < list.size()-1; i++) {
			Integer two = Integer.parseInt(list.get(i+1).substring(0,8));
			Integer one = Integer.parseInt(list.get(i).substring(0,8));
			if(two - one != 1){
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}
			//作った売上ファイルのリストを読み込む
			try {
					//listの要素数はlist.sizeで数える。ひとつずつ読む
				for(int i = 0; i <list.size(); i++) {
					String filename =list.get(i);
					File file = new File(args[0],filename);
					FileReader fr2 = new FileReader(file);
					br = new BufferedReader(fr2);
						//forの前に作ると二回目以降どんどん増えていく
					List<String> saleList = new ArrayList<String>();
					String line;
						//読み込んだファイルを１行ずつ出力する
					while((line = br.readLine()) !=null){
						saleList.add(line);
					}
					if(saleList.size() != 2) {
						System.out.println(filename + "のフォーマットが不正です" );
						return;
					}
					if(!nameMap.containsKey(saleList.get(0))) {
						System.out.println(filename + "の支店コードが不正です");
						return;
					}
					if (!saleList.get(1).matches("^[0-9]*$")){
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
					Long l = Long.parseLong(saleList.get(1)) + saleMap.get(saleList.get(0));
					if(l > 9999999999L){
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					saleMap.put(saleList.get(0),l);
				}
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
			} finally {
					if(br!=null) {
						try {
							br.close();
						}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						}
					}
			}
			BufferedWriter bw = null;
			try {
				File file = new File(args[0], "branch.out");
				FileWriter fw = new FileWriter(file);
				bw = new BufferedWriter(fw);
				for (Map.Entry<String,Long> entry : saleMap.entrySet()) {
					bw.write(entry.getKey() + "," + nameMap.get(entry.getKey()) + "," + entry.getValue());
					bw.newLine();
				}
			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
			} finally {
					if(bw != null) {
						try {
							bw.close();
						}catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						}
					}
			}
			if(!output(args[0], "branch.out", nameMap, saleMap)) {
				return;
			}
	}

	public static boolean output(String path, String filename, Map<String,String>codename, Map<String,Long>  saleMap) {
		BufferedWriter bw = null;
		try {
			File file = new File(path, filename);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for (Map.Entry<String,Long> entry : saleMap.entrySet()) {
				bw.write(entry.getKey() + "," + codename.get(entry.getKey()) + "," + entry.getValue());
				bw.newLine();
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
				if(bw != null) {
					try {
						bw.close();
					}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
					}
				}
		}
		return true;
	}
}

